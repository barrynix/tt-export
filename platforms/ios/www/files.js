function UPDATE_PROFILES()
{
  $('#profiles').find('ul').empty();
  APP_DATA.profiles.forEach(function(profile,profile_id){
    $('#profiles').find('ul').append("<li class='player-profile' data-profile-id='" + profile.profile_id + "'>" + profile.profile_name + "</li>");
  });
}

function LOAD_APP_DATA()
{
  NativeStorage.getItem("tt_data",function(data){
    APP_DATA = data;
    UPDATE_PROFILES();
  },function(){
      SAVE_APP_DATA();
  });
}

function SAVE_APP_DATA()
{
  NativeStorage.setItem("tt_data",APP_DATA,function(){
    console.log('storage_success');
  },
      function(){
    console.log('storage failure');
      });
}

function WRITE_TEXT_FILE(fileName,text)
{
  var byteData = new Blob([text], {type : 'text/plain'});

  WRITE_FILE(fileName,byteData);
}

function WRITE_IMAGE(fileName,data)
{
  var byteData = b64toBlob(data,'image/png');

  WRITE_FILE(fileName,byteData);
}

function WRITE_FILE(fileName, data, callback) {
  fileSystem.root.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
    fileEntry.createWriter(function (fileWriter) {
      fileWriter.onwriteend = function() {
        console.log("Successful file write...");
        if(typeof(callback) == 'function')
        {
          callback();
        }
      };

      fileWriter.onerror = function (e) {
          console.log("Failed file write: " + e.toString());
      };

      fileWriter.write(data);
    });
  }, function(e){ console.log('File Creation Problem:',e); });
}

function GET_CHAPTER_INDEX(part,chapter)
{
  var chapter_array = APP_DATA.profiles[CURRENT_PROFILE_INDEX]["pt" + part + "_chapters"];
  var chapter_index = -1;

  if(part == 2)
  {
    chapter -= 5;
  }

  //turn this into a regular for if we expand
  chapter_array.forEach(function(c,i){
    if(c.chapter == chapter)
    {
      chapter_index = i;
    }
  });

  return chapter_index;
}
