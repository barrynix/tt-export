window.play_yay_sfx = function()
{
  var yay_sfx = new Media('app_mp3/ding.wav', function(){ console.log('media loaded') }, function(e){ console.log(e) });
  yay_sfx.play();
}

window.play_aww_sfx = function()
{
  //var aww_sfx = new Media('app_mp3/aww.mp3', function(){ console.log('media loaded') }, function(e){ console.log(e) });
  //aww_sfx.play();
}

window.isMultipleChoiceChapter = function(chapter_id)
{
  return MULTIPLE_CHOICE_CHAPTERS.indexOf(chapter_id) != -1;
}

window.isStoryTellerChapter = function(chapter_id)
{
  return STORY_TELLER_CHAPTERS.indexOf(chapter_id) != -1;
}

window.isCalculatorChapter = function(chapter_id)
{
  return CALCULATOR_CHAPTERS.indexOf(chapter_id) != -1;
}

window.setQuestionType = function(chapter_id)
{
  if(isMultipleChoiceChapter(chapter_id))
  {
    question_type = MULTIPLE_CHOICE;
  }

  if(isStoryTellerChapter(chapter_id))
  {
    question_type = STORY_TELLER;
  }

  if(isCalculatorChapter(chapter_id))
  {
    question_type = CALCULATOR
  }
}

$.ajax({
  method : "GET",
  url : "base_profile.json",
  success : function(data)
  {
    BASE_PROFILE_OBJECT = JSON.parse(data);
  }
});

document.addEventListener('deviceready',function(){
  $(document).ready(function(){

    setTimeout(function(){
      LOAD_APP_DATA();
    }, 200);

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
      fileSystem = fs;
    }, null);
  });
}, false);
