$('body').on('touchstart','.remove_answer',function(){
		--cur_answer;

		if(cur_answer == 0)
		{
			cur_answer = 1;
            story_teller_correct = true;
		}

		$('.current_multiple_answer').removeClass('current_multiple_answer');
    $('.chapter_question[data-question-id=' + cur_question + ']').find('.multiple_answer[data-answer-id=' + cur_answer +  ']').addClass('current_multiple_answer').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
	});

$('.root_return').off().on('touchstart',function(){

	inChapter = false;
  $('body').find('.answer_button').unbind('touchstart');
  $('body').find('.next_question').unbind('touchstart');
  $('#modal_overlay').fadeOut();
  $('#quiz_results_modal').fadeOut();
	$('#demo_countdown').fadeOut();
	fading = true;

  $('#chapter_frame').fadeOut().promise().done(function(){
    UPDATE_HOME();
    $('#chapter_frame').empty();

    if(demo_mode)
    {
      $('#demo_page').fadeIn();
			fading = false;
    }else{
      $('#home_page').fadeIn().promise().done(function(){
				fading = false;
			});
    }

		setTimeout(function(){
			count_remaining = 10;
			$('#chapter_frame').css('background-image','url(img/quiz_bg.jpg)');
			$('#countdown_remaining').show();
			$('#demo_return').hide();
		},500);
  });
});

//When loading pages via HTML like this... you have issues with overlapping and repeating events.....
$('.finish_quiz').off().on('touchstart',function(){
  $('.chapter_question').fadeOut().promise().done(function(){

    var correct_answers = $('.btn-warning.correct_answer').length;
    var total_questions = $('.chapter_question').length;

    $('#correct_answers').text(correct_answers);
    $('#total_questions').text(total_questions);

    $('.quiz_results').fadeIn();
  });
});

//Just makes the numpad work.
$('.numpad-button').click(function(){
    var selected_number = $(this).data('num');
    var current_answer = $(this).closest('.math_container').find('.answer_text').text();

    if(selected_number >= 0)
    {
      current_answer += selected_number;
      $(this).closest('.math_container').find('.answer_text').text(current_answer);
    }

    if(selected_number == -1)
    {
      var supplied_answer = $('.answer_text').text();
      $('.answer_text').text(supplied_answer.substring(0,supplied_answer.length - 1));
    }

    if(selected_number == -2)
    {

      var correct_answer = answers[cur_question];

      ++cur_question;

      var supplied_answer = parseInt($('.answer_text').text());

			if(correct_answer == supplied_answer)
      {
        show_correct_answer();
      }
      else
      {
        show_incorrect_answer();
      }

      if(cur_question < questions.length)
      {
        var question = questions[cur_question];

        $('.math_container').css("background-image","url(" + question + ")");
        $('.answer_text').text("");
      }
      else
      {
        check_answers();
      }
    }
  });

  $('.numpad-button').each(function(){
    $(this).height($(this).innerWidth()/3);
  });

  //Just a shuffle function
  window.shuffle = function(array) {
    var counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        var index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        var temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

//Actually shuffle the answer bars HTML.
//Grab the elements, empty it, shuffle em, re-add
window.shuffle_answer_bar = function(next_question)
{
  $('.answer_button').unbind('touchstart');
  var answer_bar_container = next_question.find('.answer_bar');
  var question_container = $(next_question).closest('.chapter_question');
  var answer_bar = next_question.find('.answer_bar').find('.col-xs-8');
  var answers = answer_bar.find('.col-xs-4');
  var shuffled_answers = shuffle(answers);


		answer_bar.empty();

  shuffled_answers.each(function(){
        $(this).find('.answer_button').bind('touchstart',doAnswerButton);
        answer_bar.append($(this));
  });

    var answer_bar_height = 90;

    var image_height = viewportHeight - answer_bar_height;

    console.log($("#chapter_frame").height());
    console.log($("#chapter_frame"));

    $(question_container).find('.question_image').css('height',image_height).css("text-align","center");
    $(question_container).find('.question_image').find('img').css({
        'width':'auto',
        'height' : image_height,
        "position" : "relative",
        "left" : "0px",
        "top" : "0px"
    });

};

//You just need to read this to get it... I'm sorry
window.check_answers = function()
{
	var percentage = 0;
	var modal_title = "";
	var cq = window.correct_questions;
	var ca = 0;
	var tq = 0;

	var selected_part_index = 0;

	if(!demo_mode)
	{
		selected_part_index = GET_CHAPTER_INDEX(selected_part,selected_chapter);
		APP_DATA.profiles[CURRENT_PROFILE_INDEX]["pt" + selected_part + "_chapters"][selected_part_index].finished = 1;
	}

	if(question_type == CALCULATOR)
	{
		tq = questions.length;
		ca = cq;

		if(ca > tq)
		{
			ca = tq;
		}

		if(!demo_mode)
		{
			APP_DATA.profiles[CURRENT_PROFILE_INDEX]["pt" + selected_part + "_chapters"][selected_part_index].untimedCorrect = cq;
		}

		percentage = ca / tq;
	}else{
		//Make the calculation of chapter_questions based on the currently loaded chapter. It's pulling in the wrong number.
		ca = cq;
	  tq = $('.chapter_question').length;

		if(ca > tq)
		{
			ca = tq;
		}

	  percentage = cq / tq;

		if(!demo_mode)
		{
			APP_DATA.profiles[CURRENT_PROFILE_INDEX]["pt" + selected_part + "_chapters"][selected_part_index].untimedCorrect = cq;
		}

	  modal_title = "Good Job!";
	}

  if(percentage < 0.8)
  {
    modal_title = "Oh no!";
    $('#quiz_failure_text').show();
  }else {
    if(demo_mode)
    {
      if(APP_DATA.demo_progress < 5)
      {
        ++APP_DATA.demo_progress;
        SAVE_APP_DATA();
        UPDATE_HOME();
      }
    }else{

			APP_DATA.profiles[CURRENT_PROFILE_INDEX]["pt" + selected_part + "_chapters"][selected_part_index].passed = 1;

			if(next_part != null && next_chapter != null)
			{
	      var next_part_index = GET_CHAPTER_INDEX(next_part,next_chapter);
	      APP_DATA.profiles[CURRENT_PROFILE_INDEX]["pt" + next_part + "_chapters"][next_part_index].locked = 0;
	      SAVE_APP_DATA();
	      UPDATE_HOME();
			}
    }
  }

	if(ca > tq)
	{
		ca = tq;
	}

  $('#quiz_result_title').text(modal_title);
  $('#correct_answers').text(ca);
  $('#total_answers').text(tq);

	if(window.finalLevel)
	{
		if(demo_mode)
		{
			scorescreen = 'app_sample/APPDownloadFullVersion.jpg';
		}else{
			scorescreen = 'app_img/game_complete.jpg';
		}
	}

	$('.math_container').hide();
	$('.answer_bar').hide();

	$('#chapter_frame').css('background-image','url(' + scorescreen + ')');

	if(window.finalLevel && demo_mode)
	{
		$('#demo_countdown').show();

		var int_id = setInterval(function () {
			if(count_remaining > 0)
			{
				$('#countdown_remaining').text(count_remaining);
				--count_remaining;
			}else{
				clearInterval(int_id);
				$('#countdown_remaining').hide();
				$('#demo_return').show();
			}
		}, 1000);
	}else{
		$('#modal_overlay').fadeIn();
	  $('#quiz_results_modal').fadeIn();
	}

	scorescreen = 'app_img/ScoreScreen.jpg';
};
