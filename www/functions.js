//Here's another thing I'd re-do
//Depending on the type of question, check the answer in different ways.
window.doAnswerButton = function(){
  var button = $(this);
  //Multiple choice answers have the correct answer marked.
  //Did they pick the right one? 
  if(question_type == MULTIPLE_CHOICE)
  {
    $(button).closest('.answer_bar').find('.answer_button').removeClass('btn-warning').addClass('btn-success');
    $(button).addClass('btn-warning').removeClass('btn-success');

    correct_answer = $(button).hasClass('correct_answer');
  }

  //The story teller quiz just needs the answers in the right order.
  //If it's not... the end then add whatever text is in the button and add it in its spot.
  //A game engine would have been helpful here...
  if(question_type == STORY_TELLER)
  {
    $('.chapter_question[data-question-id=' + cur_question + ']').find('.multiple_answer[data-answer-id=' + cur_answer +  ']').text($(this).text());

    if($(button).attr('data-answer') != cur_answer)
    {
      story_teller_correct = false;
    }
    ++cur_answer;

    $('.current_multiple_answer').removeClass('current_multiple_answer');
    $('.chapter_question[data-question-id=' + cur_question + ']').find('.multiple_answer[data-answer-id=' + cur_answer +  ']').addClass('current_multiple_answer');

    if(cur_answer > 3)
    {
      if(story_teller_correct)
      {
        show_correct_answer();
        cur_answer = 1;
      }
      else
      {
        show_incorrect_answer();
        cur_answer = 1;
      }

      var total_chapter_questions = $('.chapter_question').length;

      if(total_chapter_questions >= cur_question + 1)
      {
        $(this).closest('.chapter_question').fadeOut().promise().done(function(){
          ++cur_question;
          cur_answer = 1;
          story_teller_correct = true;
          $('.current_multiple_answer').removeClass('current_multiple_answer');
          shuffle_answer_bar($('.chapter_question[data-question-id=' + cur_question + ']'));
          $('.chapter_question[data-question-id=' + cur_question + ']').find('.multiple_answer[data-answer-id=' + cur_answer +  ']').addClass('current_multiple_answer');
          $('.chapter_question[data-question-id=' + cur_question + ']').fadeIn();
        });
      }
      else
      {
        check_answers();
      }
    }
  }
};


window.doNextQuestion = function(){

  var original_element = $(this);

  if(question_type == MULTIPLE_CHOICE)
  {
    $('.chapter_video').hide();

    if(!$(original_element).hasClass('to_questions'))
    {
      if(correct_answer)
      {
        show_correct_answer();
      }
      else
      {
        show_incorrect_answer();
      }
    }

    $('.chapter_question').fadeOut().promise().done(function(){
      if(!$(original_element).hasClass('to_questions'))
      {
        ++cur_question;
      }

      var next_question = $('#chapter_frame').find('.chapter_question[data-question-id=' + cur_question + ']');

      if(next_question.length != 0)
      {
        correct_answer = false;

        shuffle_answer_bar(next_question);

        next_question.fadeIn();
      }
      else
      {
        check_answers();
      }
    });
  }

  if(question_type == CALCULATOR)
  {

    if($(original_element).hasClass('to_questions'))
    {
      $('.chapter_video').fadeOut();
      $('.math_chapter_container').fadeIn();
    }
  }

  if(question_type == STORY_TELLER)
  {
    $(this).closest('.chapter_question').fadeOut().promise().done(function(){
      ++cur_question;

      var next_question = $('#chapter_frame').find('.chapter_question[data-question-id=' + cur_question + ']');

      if(next_question.length != 0)
      {
        correct_answer = false;

        shuffle_answer_bar(next_question);

        next_question.fadeIn();
      }
      else
      {
        check_answers();
      }
    });
  }
};
