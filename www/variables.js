//QUESTION TYPES
const MULTIPLE_CHOICE = 1;
const STORY_TELLER = 2;
const CALCULATOR = 3;

//WHAT CHAPTERS ARE WHICH?
const MULTIPLE_CHOICE_CHAPTERS = [1,2,6]
const STORY_TELLER_CHAPTERS = [3,7];
const CALCULATOR_CHAPTERS = [4,5,8,9];

var questions = [];
var selected_question = 0;
var timed_quiz = false;
var selected_chapter = 0;
var selected_part = 0;
var next_chapter = 0;
var next_part = 0;
var cur_question = 1;
var cur_answer = 1;
var quiz_type = 0;
var question_type = MULTIPLE_CHOICE;
var correct_answer = false;
var story_teller_correct = true;

var BASE_PROFILE_OBJECT = null;

//THE BASE APP DATA PROJECT
var APP_DATA = { "profiles" : [] , "demo_progress" : 1 };

var CURRENT_PROFILE_ID = -1;
var CURRENT_PROFILE_INDEX = -1;
var fileSystem = null;

//Is it a demo?
var demo_mode = false;
var mobile_mode = true;

var homescreenSample = 'app_img/HomeScreenFreeSample.jpg';
var homescreenFull = 'app_img/HomeScreenFullVersion.jpg';
var scorescreen = 'app_img/ScoreScreen.jpg';
var buyfull = 'app_img/BuytheFullVersion.jpg';

var showing_profile_select = true;

var count_remaining = 10;
var fading = false;
var inChapter = false;

var STORAGE_CONTAINER = window.localStorage;

var viewportWidth = $(window).width();
var viewportHeight = $(window).height();

window.finalLevel = false;
