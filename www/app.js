document.addEventListener('deviceready',function(){
$(document).ready(function(){
  //setup the "sparkle" animation
  clickSpark.setParticleCount(10);
  clickSpark.setParticleSize(50);
  clickSpark.setParticleSpeed(50);
  clickSpark.setParticleImagePath('img/star.png');
  clickSpark.setParticleRotationSpeed(20);
  clickSpark.setParticleDuration(1500);
  clickSpark.setAnimationType('explosion');

  //Expansion file (legacy code) plugin
  if (window.XAPKReader) {
      //XAPKReader is an OK plugin for Cordova but I had issues with it with newer versions.
      //I recommend avoiding expansion files or writing your own plugin for this
      window.XAPKReader.downloadExpansionIfAvailable(function () {
          console.log("Expansion file check/download success.");
      }, function (err) {
          console.log(err);
          throw "Failed to download expansion file.";
      })
  }

  //Legacy code for doing in-app purchases
  $('#restore_purchase').click(function(){
      //Use the spinner plugin to lock the page and create a "restoring message"
      SpinnerPlugin.activityStart('Restoring...',null);

      inAppPurchase
      .restorePurchases()
      .then(function (data) {
        //HIde the spinner
        SpinnerPlugin.activityStop();

        if(data.length != 0)
        {
          APP_DATA.app_purchased = true;
          SAVE_APP_DATA();
        }else{
          alert("There was a problem restoring your purchase");
        }
      }).catch(function (err) {
        alert(err.errorMessage);
        SpinnerPlugin.activityStop();
      });
  });

  //Current code. When you click "buy now" it taks you to the app store page
  $('#buy_full_version_button').click(function(){
    if(!inChapter)
    {
        window.open("https://itunes.apple.com/us/app/times-tales-mobile/id1440874656?ls=1&mt=8",'_system');
    }
  });

  //When the user selects a chapter, setup the variables and load the right stuff
  $('body').on('click','.chapter_selection',function(){
    if(!$(this).hasClass('chapter_locked') && !fading)
    {
      selected_chapter = $(this).data('chapter-id');
      selected_part = $(this).data('part-id');

      next_chapter = $(this).data('next-chapter-id');
      next_part = $(this).data('next-part-id');

      setQuestionType(selected_chapter);

      $('#chapter_frame').empty();

      $('#chapter_select_modal').fadeIn();
    }
  });

  //Just shows the profile modal. BS at the time at issues with touch events
  $('#add_profile').on('touchstart',function(){
    $('#new_profile_modal').fadeIn();
  });

  //Close the modal. Same reason as above.
  $('#cancel_profile_add').on('touchstart',function(){
    $('#new_profile_modal').fadeOut();
    $('#player_name').val('');
  });

  //That "home" button at the top of the page. Basically opens up the profile selector.
  $('#profile_select_button').on('touchstart',function(){
    $('#home_page').fadeOut().promise().done(function(){
      $('#profile_select').fadeIn();
      showing_profile_select = true;
    });
  });

  //When you select a profile, set the variables
  $('body').on('touchstart','.player-profile',function(){
    var player_id = $(this).data("profile-id");

    CURRENT_PROFILE_ID = player_id;

    for(var i = 0; i < APP_DATA.profiles.length; ++i)
    {
      if(APP_DATA.profiles[i].profile_id == CURRENT_PROFILE_ID)
      {
        CURRENT_PROFILE_INDEX = i;
        break;
      }
    }

    //You'd be surprised how many times javascript/jQuery creates race conditions even though it's not supposed to...
    $('#profile_select').fadeOut().promise().done(function(){
      UPDATE_HOME();

      $('#home_page').fadeIn();
      $('#current_player_label').text("Current Player: " + APP_DATA.profiles[CURRENT_PROFILE_INDEX]["profile_name"]);
      showing_profile_select = false;
    });
  });

  $('#add_profile_button').on('touchstart',function(){
    LOAD_APP_DATA();

    setTimeout(function(){
      var player_name = $('#player_name').val();
      $('#player_name').val('');

      //Copy the template profile so that we can *create* a new one
      var new_profile_object = JSON.parse(JSON.stringify(BASE_PROFILE_OBJECT));

      var d = new Date();
      var seconds = Math.round(d.getTime() / 1000);

      new_profile_object.profile_name = player_name;
      new_profile_object.profile_id = seconds;

      APP_DATA.profiles.push(new_profile_object);

      SAVE_APP_DATA();
      UPDATE_HOME();
      UPDATE_PROFILES();
      $('#new_profile_modal').fadeOut();
      $('#player_name').val('');
    },300);
  });

  //I'd do this completely differently now
  // but I was young and naive back then...
  $('#start_chapter').click(function(){
    window.finalLevel = false;

    cur_question = 1;
    cur_answer = 1;
    window.correct_questions = 0;

    inChapter = true;
    $('#quiz_failure_text').hide();

    //Two different... kinda... behaviors depending on if it's demo mode or not.
    //Basically, determine which folder to pull from amongst a few other things.
    if(demo_mode)
    {
      $('#chapter_frame').load('demo_chapters/' + selected_chapter + '.html');
      $('#chapter_select_modal').fadeOut();
      $('#demo_page').fadeOut().promise().done(function(){
        $('body').find('.to_questions').hide();
        $('body').find('.watch_video_button').show();
        $('#chapter_frame').fadeIn();

        setTimeout(function(){
          if($('video').length != 0)
          {
            $('#chapter_frame').find('video')[0].load();
            $('#chapter_frame').find('video').on('loadeddata',function(){
              resize_video();
              setTimeout(resize_video,500);
            });

            $('#chapter_frame').find('video').on('ended',function(){
              $('.to_questions').show();
              $('.watch_video_button').hide();
            });
          }
        },400);
      });
    }else{
      $('#chapter_frame').load('chapters/' + selected_chapter + '.html');
      $('#chapter_select_modal').fadeOut();
      $('#home_page').fadeOut().promise().done(function(){

        //Remove the events or else there will be duplicates... at the time :)
        $('video').off('loadeddata');
        $('video').off('ended');
        $('body').find('.to_questions').hide();
        $('body').find('.watch_video_button').show();
        $('#chapter_frame').fadeIn();

      	var selected_part_index = GET_CHAPTER_INDEX(selected_part,selected_chapter);

      	APP_DATA.profiles[CURRENT_PROFILE_INDEX]["pt" + selected_part + "_chapters"][selected_part_index].viewed = 1;

        setTimeout(function(){
          //If there's a video load it and set a timeout between when the page is loaded and when the video is resized
          if($('video').length != 0)
          {
            $('#chapter_frame').find('video')[0].load();
            $('#chapter_frame').find('video').on('loadeddata',function(){
              resize_video();
              setTimeout(resize_video,500);
            });

            $('#chapter_frame').find('video').on('ended',function(){
              $('.to_questions').show();
              $('.watch_video_button').hide();
            });
          }
        },400);
      });
    }

    setTimeout(function(){
      $('#chapter_frame').find('.next_question').bind('touchstart',doNextQuestion);
    },500);
  });


  $('#exit_chapter_modal').click(function(){
    $('#chapter_select_modal').fadeOut();
  });

  //Adjust the video based on its container
  window.resize_video = function()
  {
    var window_width = window.innerWidth;
    var window_height = window.innerHeight;

    var video_width = $('video').width();
    var video_height = $('video').height();

    var answer_bar_height = $('.answer_bar').first().innerHeight();

    var total_height = window_height - answer_bar_height;
    var horiz_center = window_width / 2;
    var vert_center = total_height / 2;

    $('video').css('top',vert_center - (video_height/2)).css('left',horiz_center - (video_width/2));
  };

  /* 

  Basically, this goes over each chapter element in the "home page" and updates it depending on the state of the app
  
  */
  window.UPDATE_HOME = function()
  {
    if(demo_mode){

      if(!mobile_mode) {
          $('body').css('background-image','url(' + homescreenSample + ')');
      }

      for(var i = 1; i <= 5; ++i)
      {
        //This should never be null, but just in case (so the app doesn't crash)
        if(APP_DATA.demo_progress != null)
        {
          var story_chapter = $('#demo_page').find('.story_chapter[data-chapter-id=' + i + ']');

          if(i <= APP_DATA.demo_progress)
          {
            $(story_chapter).removeClass('chapter_locked');

            var chapter_stars_html = '';
            chapter_stars_html += '<div class="chapter_completion"></div>';

            $(story_chapter).find('.chapter_progress').empty().html(chapter_stars_html);
          }
          else
          {
            $(story_chapter).addClass('chapter_locked');

            var chapter_stars_html = '';
            chapter_stars_html += '<div class="locked">';
            chapter_stars_html += '<i class="fa fa-lock"></i>';
            chapter_stars_html += '</div>';

            $(story_chapter).find('.chapter_progress').empty().html(chapter_stars_html);
          }
        }
      }

    }else {

      if(showing_profile_select)
      {
        $('body,html').css('background-image','url(' + scorescreen + ')');
      }else{
        $('body,html').css('background-image','url(' + homescreenFull + ')');
      }

      if(CURRENT_PROFILE_INDEX != -1)
      {
      APP_DATA.profiles[CURRENT_PROFILE_INDEX].pt2_chapters[0].questions = 27;
      APP_DATA.profiles[CURRENT_PROFILE_INDEX].pt2_chapters[1].questions = 10;
      APP_DATA.profiles[CURRENT_PROFILE_INDEX].pt2_chapters[2].questions = 10;
      APP_DATA.profiles[CURRENT_PROFILE_INDEX].pt2_chapters[3].questions = 20;
      APP_DATA.profiles[CURRENT_PROFILE_INDEX].pt1_chapters.forEach(function(chapter,chapter_id){
        var story_chapter = $('.story_chapter[data-chapter-id=' + (chapter_id + 1) + ']');

        if(chapter.locked == 0)
        {
          $(story_chapter).removeClass('chapter_locked');

          var chapter_stars_html = '';
          chapter_stars_html += '<div class="chapter_stars">';
          chapter_stars_html += '<i class="fa fa-star chapter_viewed"></i>';
          chapter_stars_html += '<i class="fa fa-star chapter_finished"></i>';
          chapter_stars_html += '<i class="fa fa-star chapter_passed"></i>';
          chapter_stars_html += '</div>';
          chapter_stars_html += '<div class="chapter_completion">' + chapter.untimedCorrect + "/" + chapter.questions +  '</div>';

          $(story_chapter).find('.chapter_progress').empty().html(chapter_stars_html);

          if(chapter.viewed != null && chapter.viewed == 1)
          {
            $(story_chapter).find('.chapter_progress').find('.chapter_viewed').css('color','yellow');
          }

          if(chapter.finished != null && chapter.finished == 1)
          {
            $(story_chapter).find('.chapter_progress').find('.chapter_finished').css('color','yellow');
          }

          if(chapter.passed != null && chapter.passed == 1)
          {
            $(story_chapter).find('.chapter_progress').find('.chapter_passed').css('color','yellow');
          }
        }
        else
        {
          $(story_chapter).addClass('chapter_locked');

          var chapter_stars_html = '';
          chapter_stars_html += '<div class="locked">';
          chapter_stars_html += '<i class="fa fa-lock"></i>';
          chapter_stars_html += '</div>';

          $(story_chapter).find('.chapter_progress').empty().html(chapter_stars_html);
        }
    });

    APP_DATA.profiles[CURRENT_PROFILE_INDEX].pt2_chapters.forEach(function(chapter,chapter_id){
      ++chapter_id;

      var story_chapter = $('.story_chapter[data-chapter-id=' + (chapter_id + APP_DATA.profiles[CURRENT_PROFILE_INDEX].pt1_chapters.length) + ']');

      if(chapter.locked == 0)
      {
        $(story_chapter).removeClass('chapter_locked');

        var chapter_stars_html = '';
        chapter_stars_html += '<div class="chapter_stars">';
        chapter_stars_html += '<i class="fa fa-star chapter_viewed"></i>';
        chapter_stars_html += '<i class="fa fa-star chapter_finished"></i>';
        chapter_stars_html += '<i class="fa fa-star chapter_passed"></i>';
        chapter_stars_html += '</div>';
        chapter_stars_html += '<div class="chapter_completion">' + chapter.untimedCorrect + "/" + chapter.questions +  '</div>';

        $(story_chapter).find('.chapter_progress').empty().html(chapter_stars_html);

        if(chapter.viewed != null && chapter.viewed == 1)
        {
          $(story_chapter).find('.chapter_progress').find('.chapter_viewed').css('color','yellow');
        }

        if(chapter.finished != null && chapter.finished == 1)
        {
          $(story_chapter).find('.chapter_progress').find('.chapter_finished').css('color','yellow');
        }

        if(chapter.passed != null && chapter.passed == 1)
        {
          $(story_chapter).find('.chapter_progress').find('.chapter_passed').css('color','yellow');
        }
      }
      else
      {
        $(story_chapter).addClass('chapter_locked');

        var chapter_stars_html = '';
        chapter_stars_html += '<div class="locked">';
        chapter_stars_html += '<i class="fa fa-lock"></i>';
        chapter_stars_html += '</div>';

        $(story_chapter).find('.chapter_progress').empty().html(chapter_stars_html);
      }
    });
  }
  }
};

  $(window).resize(function(){
    resize_video();
  });

  window.show_incorrect_answer = function()
  {
    play_aww_sfx();
    $('#incorrect_answer').addClass('bounceIn').show();

    setTimeout(function(){
      $('#incorrect_answer').fadeOut();
    },800);
  };

  window.show_correct_answer = function()
  {
    ++window.correct_questions;
    play_yay_sfx();
    $('#correct_answer').addClass('bounceIn').show();
    clickSpark.fireParticles($('#correct_answer'));

    setTimeout(function(){
      $('#correct_answer').fadeOut();
    },800);
  };

  setTimeout(function(){
    LOAD_APP_DATA();

    if(demo_mode)
    {
      $('#profile_select').hide();
      $('#home_page').hide();
      $('#demo_page').show();
    }else {
      $('#profile_select').show();
      $('#home_page').hide();
      $('#demo_page').hide();
      showing_profile_select = true;
    }

    UPDATE_HOME();
  },500);
});

var sizeFrame = function(){
  var wWidth = window.outerWidth;
  var wHeight = window.outerHeight;

  $('#chapter_frame').css({
      width : wWidth,
      height : wHeight
  });
};

setInterval(sizeFrame,100);

$(window).resize(sizeFrame);

}, false);
